-- migration story

drop table if exists #FTA_sp_pcp_history;
select
    Fiscal_year_mth, 
    MTH_START_DATE,
    CUSTOMER_KEY,
    SID_contract_num, 
    SFDC_type,
    site_type,
    OPPORTUNITY_ID as opp_number,
    SF_OPPORTUNITY_ID as opportunity_id,
    ACCOUNT_HEALTH_BUCKET,
    HEALTH_SCORE,
    case when CSM_COVERED = 'x' then 1 else 0 end as CSM_COVERED,
    INTERNAL_INDUSTRY_CLASSIFICATION,
    REPORTING_REGION,
    contract_sign_dt,
    FISCAL_YRMTH_contract_line_start, 
    FISCAL_YRMTH_CONTRACT_LINE_END, 
    GTM_CATEGORY,
    product_pillar,
    case 
        when [PRODUCT_PILLAR] in ('Skillsoft', 'VODECLIC') then 1
        else 0
    end as Skillport_flag,
    case 
        when [PRODUCT_PILLAR] in ('Percipio') then 1
        else 0
    end as Percipio_flag,
    sum(OI_AMT_USD_AT_2022_RATE) as contracted_oi_current_rate
    -- sum(CONTRACTED_OI_AMT_USD_BOOKED_AT_CURRENT_RATE) as contracted_oi
into #FTA_sp_pcp_history
from dwbi.dbo.FACT_FTA_CONTRACT_MONTH_NIGHTLY
where PRODUCT_PILLAR in ('PERCIPIO', 'SKILLSOFT','VODECLIC')
and PRODUCT_TYPE_DETAIL = 'RECURRING'
and PRODUCT_DIVISION = 'TOTAL CONTENT'
-- and fiscal_year_mth >= 'FY2020-P01'
group by     
    Fiscal_year_mth, 
    MTH_START_DATE,
    CUSTOMER_KEY,
    SID_contract_num, 
    SFDC_type,
    site_type,
    OPPORTUNITY_ID,
    SF_OPPORTUNITY_ID,
    ACCOUNT_HEALTH_BUCKET,
    HEALTH_SCORE,
    CSM_COVERED,
    INTERNAL_INDUSTRY_CLASSIFICATION,
    REPORTING_REGION,
    contract_sign_dt,
    FISCAL_YRMTH_contract_line_start, 
    FISCAL_YRMTH_CONTRACT_LINE_END, 
    GTM_CATEGORY,
    product_pillar
order by 
    CUSTOMER_KEY, 
    FISCAL_YEAR_MTH, 
    SID_contract_num
;


select 
    fiscal_year_mth, 
    product_pillar,
    sum(OI_AMT_USD_BOOKED),
    sum(OI_AMT_USD_AT_2022_RATE),
    --======================================
    sum(CONTRACTEDVALUE_BOOKED_CURRENT) * 12
    --======================================
from dwbi.dbo.FACT_FTA_CUST_PRODUCTS_NIGHTLY
-- from dwbi.dbo.FACT_FTA_CONTRACT_MONTH_NIGHTLY
where PRODUCT_PILLAR in ('PERCIPIO', 'SKILLSOFT','VODECLIC')
and PRODUCT_TYPE_DETAIL = 'RECURRING'
and PRODUCT_DIVISION = 'TOTAL CONTENT'
and FISCAL_YEAR_MTH = 'FY2022-P03'
group by fiscal_year_mth, PRODUCT_PILLAR
order by FISCAL_YEAR_MTH
;


-- select 
--     fiscal_year_mth, 
--     product_pillar,
--     sum(contracted_oi_current_rate)
-- from #FTA_sp_pcp_history
-- where PRODUCT_PILLAR in ('PERCIPIO', 'SKILLSOFT','VODECLIC')
-- and FISCAL_YEAR_MTH = 'FY2022-P03'
-- group by fiscal_year_mth, PRODUCT_PILLAR
-- order by FISCAL_YEAR_MTH
-- ;



-- ================================================================================================
-- customer info and summary table creation

-- platform change history summary
drop table if exists sandbox.dbo.rg_FTA_customer_platform_change_history_summary;
select 
    *, 
    case 
        when skillport_flag = 1 and percipio_flag = 0 then 'SP'
        when skillport_flag = 0 and percipio_flag = 1 then 'PCP'
        when skillport_flag = 1 and percipio_flag = 1 then 'SP + PCP'
    end as product_platform, 
    rank() over (partition by customer_key order by min_MTH_START_DATE) as platform_rank
into sandbox.dbo.rg_FTA_customer_platform_change_history_summary
from     
(
    select 
        CUSTOMER_KEY,
        skillport_flag,
        percipio_flag,
        min(MTH_START_DATE) as min_MTH_START_DATE,
        max(MTH_START_DATE) as max_MTH_START_DATE
    from
    (
        select 
            MTH_START_DATE,
            CUSTOMER_KEY,
            max(Skillport_flag) as skillport_flag,
            max(percipio_flag) as percipio_flag
        from #FTA_sp_pcp_history
        group by MTH_START_DATE, CUSTOMER_KEY
    ) a
    group by     
        CUSTOMER_KEY,
        skillport_flag,
        percipio_flag
) b
order by CUSTOMER_KEY, min_MTH_START_DATE, max_MTH_START_DATE
;


drop table if exists #FTA_customer_last_platform;
select 
    a.CUSTOMER_KEY,
    a.product_platform as last_product_platform,
    b.last_platform_rank,
    a.min_MTH_START_DATE as last_platform_start_mth_date,
    a.max_MTH_START_DATE as last_platform_end_mth_date
into #FTA_customer_last_platform
from 
(
    select 
        customer_key,
        product_platform,
        platform_rank,
        min_MTH_START_DATE,
        max_MTH_START_DATE
    from sandbox.dbo.rg_FTA_customer_platform_change_history_summary
) a
LEFT JOIN
(
    select 
        customer_key, 
        max(platform_rank) as last_platform_rank
    from sandbox.dbo.rg_FTA_customer_platform_change_history_summary
    group by customer_key
) b
ON a.customer_key = b.CUSTOMER_KEY
where platform_rank = last_platform_rank
order by customer_key
;



drop table if exists #FTA_customer_platform_order_and_info;
select 
    CUSTOMER_KEY, 
    max(first_product_platform) as first_product_platform,
    max(first_product_platform_min_MTH_START_DATE) as first_product_platform_start_MTH_START_DATE,
    max(first_product_platform_max_MTH_START_DATE) as first_product_platform_end_MTH_START_DATE,

    max(second_product_platform) as second_product_platform,
    max(second_product_platform_min_MTH_START_DATE) as second_product_platform_start_MTH_START_DATE,
    max(second_product_platform_max_MTH_START_DATE) as second_product_platform_end_MTH_START_DATE,

    max(third_product_platform) as third_product_platform,
    max(third_product_platform_min_MTH_START_DATE) as third_product_platform_start_MTH_START_DATE,
    max(third_product_platform_max_MTH_START_DATE) as third_product_platform_end_MTH_START_DATE,

    min(case when skillport_flag = 1 then min_MTH_START_DATE end) as skillport_first_month,
    min(case when percipio_flag = 1 then min_MTH_START_DATE end) as percipio_first_month

into #FTA_customer_platform_order_and_info
from 
(
    select 
        *,
        case when platform_rank = 1 then product_platform end as first_product_platform,
        case when platform_rank = 1 then min_MTH_START_DATE end as first_product_platform_min_MTH_START_DATE,
        case when platform_rank = 1 then max_MTH_START_DATE end as first_product_platform_max_MTH_START_DATE,

        case when platform_rank = 2 then product_platform end as second_product_platform,
        case when platform_rank = 2 then min_MTH_START_DATE end as second_product_platform_min_MTH_START_DATE,
        case when platform_rank = 2 then max_MTH_START_DATE end as second_product_platform_max_MTH_START_DATE,

        case when platform_rank = 3 then product_platform end as third_product_platform,
        case when platform_rank = 3 then min_MTH_START_DATE end as third_product_platform_min_MTH_START_DATE,
        case when platform_rank = 3 then max_MTH_START_DATE end as third_product_platform_max_MTH_START_DATE

    from sandbox.dbo.rg_FTA_customer_platform_change_history_summary
) a
group by CUSTOMER_KEY
order by CUSTOMER_KEY
;



drop table if exists #FTA_customer_platform_summary;
select 
    a.CUSTOMER_KEY,
    skillport_first_month,
    percipio_first_month,

    first_product_platform,
    first_product_platform_start_MTH_START_DATE,
    first_product_platform_end_MTH_START_DATE,

    second_product_platform,
    second_product_platform_start_MTH_START_DATE,
    second_product_platform_end_MTH_START_DATE,

    third_product_platform,
    third_product_platform_start_MTH_START_DATE,
    third_product_platform_end_MTH_START_DATE,

    last_product_platform,
    last_platform_rank,
    last_platform_start_mth_date,
    last_platform_end_mth_date
into #FTA_customer_platform_summary
from #FTA_customer_platform_order_and_info a
left join #FTA_customer_last_platform b
on a.customer_key = b.customer_key
order by customer_key
;
-- select 
--     customer_key,
--     max(min_MTH_START_DATE) as last_platform_start_mth_date,
--     max(max_MTH_START_DATE) as last_platform_end_mth_date,
--     max(platform_rank) as last_platform_rank
-- from sandbox.dbo.rg_FTA_customer_platform_change_history_summary
-- group by customer_key
-- order by customer_key
-- ;

-- select 
--     top 100 *
-- from sandbox.dbo.rg_FTA_customer_platform_change_history_summary
-- order by customer_key
-- ;

-- select 
--     -- top 100 *
--     CUSTOMER_KEY,
--     max(platform_rank) as last_platform_rank
-- from sandbox.dbo.rg_FTA_customer_platform_change_history_summary
-- order by customer_key
-- ;

-- select top 100 *
-- from #FTA_customer_platform_summary
-- order by customer_key
-- ;

-- ================================================================================================















-- FINAL TABLE=================================================================================
drop table if exists sandbox.dbo.rg_FTA_customer_platform_history_info;
select 
    a.*,
    b.skillport_first_month,
    b.percipio_first_month,

    b.first_product_platform,
    b.first_product_platform_start_MTH_START_DATE,
    b.first_product_platform_end_MTH_START_DATE,

    b.second_product_platform,
    b.second_product_platform_start_MTH_START_DATE,
    b.second_product_platform_end_MTH_START_DATE,

    b.third_product_platform,
    b.third_product_platform_start_MTH_START_DATE,
    b.third_product_platform_end_MTH_START_DATE,

    b.last_product_platform,
    b.last_platform_rank,
    b.last_platform_start_mth_date,
    b.last_platform_end_mth_date

into sandbox.dbo.rg_FTA_customer_platform_history_info
from #FTA_sp_pcp_history a
LEFT JOIN #FTA_customer_platform_summary b
on a.CUSTOMER_KEY = b.CUSTOMER_KEY
;

-- select top 10 *
-- from sandbox.dbo.rg_FTA_customer_platform_history_info
-- order by CUSTOMER_KEY, FISCAL_YEAR_MTH
-- ;
-- =========================================================================================


































select 
    fiscal_year_mth, 
    Skillport_flag, 
    Percipio_flag, 
    product_pillar,
    sum(contracted_oi_current_rate)
from sandbox.dbo.rg_FTA_customer_platform_history_info
where FISCAL_YEAR_MTH = 'FY2022-P03'
group by fiscal_year_mth, Skillport_flag, Percipio_flag, PRODUCT_PILLAR
order by FISCAL_YEAR_MTH
;


select top 10 *
from sandbox.dbo.rg_FTA_customer_platform_history_info
;



select 
    node_id,
    contract_num,
    site_name, 
    case when eligible = 'Yes' then 1 else 0 end as eligibility,
    migration_status,
    migration_date,
    loadDate
from dwbi.dbo.skillport_site_eligibility
where loadDate = '2021-04-25 18:00:11.363'
and site_type in ('Production Customer', 'Shared - Inside Sales', 'Shared - Other')
AND contract_num != ''
;

select contract_num, count(*) as cnt
from dwbi.dbo.skillport_site_eligibility
where loadDate = '2021-04-25 18:00:11.363'
and site_type in ('Production Customer', 'Shared - Inside Sales', 'Shared - Other')
-- AND contract_num != ''
group by contract_num
order by CONTRACT_NUM ASC
;

select distinct 
    eligible, 
    migration_status
    -- ,migration_date
from dwbi.dbo.skillport_site_eligibility
order by eligible, migration_status
;









select top 10 * 
from  dwbi.dbo.percipio_migration_date
where load_date = '2021-04-26 12:08:33.723'
and migration_date <= '2022-01-01'
;

select top 10 
    SITE_ID,
    SITE_PERCIPIO_ORGUUID,
    SITE_SNAME,
    SITE_TRAININGACADEMY_ORGID,
    SITE_TYPE
from dwbi.dbo.FACT_FTA_CONTRACT_MONTH_NIGHTLY
;

select distinct SITE_TYPE
from dwbi.dbo.FACT_FTA_CONTRACT_MONTH_NIGHTLY
;

select distinct migration_status
from  dwbi.dbo.percipio_migration_date
-- where load_date = '2021-04-26 12:08:33.723'
-- and migration_date <= '2022-01-01'
-- order by load_date
;























-- =========================================================================================
-- table exploration


select top 10 *
from dwbi.dbo.skillport_site_eligibility
;

select distinct loaddate
from dwbi.dbo.skillport_site_eligibility
order by loaddate
;

select *
from dwbi.dbo.skillport_site_eligibility
where loadDate = '2020-11-10 17:09:28.330'
;

select distinct load_date
from  dwbi.dbo.percipio_migration_date
order by load_date
;



select contract_num, count(*) as cnt
from dwbi.dbo.skillport_site_eligibility
where loadDate = '2021-05-03 11:16:07.523'
and site_type in ('Production Customer', 'Shared - Inside Sales', 'Shared - Other')
group by contract_num
order by cnt DESC
;

select 
    loaddate,
    COUNT(distinct contract_num) as cnt_contract,
    COUNT(distinct node_id) as cnt_site_id
from dwbi.dbo.skillport_site_eligibility
-- where loadDate = '2021-04-25 18:00:11.363'
where site_type in ('Production Customer', 'Shared - Inside Sales', 'Shared - Other')
group by loadDate
order by loaddate
;

-- what is the estimate of number of SP sites that we have?









--  account health table
-- is it assigned to CSM
-- how active are the admins
-- skillport historic eligilibility shift -> data available?
-- (from inegilibilty to eligibility)

select top 10 *
from dwbi.dbo.FACT_Aggr_CUST_Account_Health_By_Month
order by DUNS_NUMBER
;

select top 10 *
from dwbi.dbo.FACT_SF_ACCOUNT_HEALTH_KPI
;

select top 100 SF_OPPORTUNITY_ID, HEALTH_SCORE, ACCOUNT_HEALTH_BUCKET, CSM_covered, *
from dwbi.dbo.FACT_FTA_CUST_PRODUCTS_NIGHTLY
where CSM_COVERED is null /* and HEALTH_SCORE is not null */
;

select top 10 *
from dw.dbo.FACT_SF_ACCOUNT_HEALTH__C
;


-- vafle table for usage by platform

select top 10 * 
from UsageDWBI.dbo.fact_aggr_cust_monthly_usage_by_platform
;

select 
    firstdayofmonth,
    platform,
    count(distinct customer_key) as cnt_d_customer,
    count(distinct site) as cnt_d_site
from UsageDWBI.dbo.fact_aggr_cust_monthly_usage_by_platform
where platform != 'TA'
group by firstdayofmonth, platform
order by platform, firstdayofmonth
;

select 
    firstdayofmonth,
    platform,
    customer_key,
    [company_name],
    [site],
    Associated_url,
    base_engagement_ind
from UsageDWBI.dbo.fact_aggr_cust_monthly_usage_by_platform
where platform != 'TA'
order by platform, firstdayofmonth
;



select 
distinct     
    PRODUCT_SEGMENT, 
    PRODUCT_DIVISION,
    product_pillar
-- from dwbi.dbo.FACT_FTA_CUST_PRODUCTS_NIGHTLY 
from dwbi.dbo.FACT_FTA_CONTRACT_MONTH_NIGHTLY
where PRODUCT_TYPE_DETAIL = 'RECURRING'
and PRODUCT_DIVISION = 'TOTAL CONTENT'
order by product_pillar
;


select 
distinct     
    PRODUCT_SEGMENT, 
    PRODUCT_DIVISION,
    product_pillar
from dwbi.dbo.FACT_FTA_CONTRACT_MONTH_NIGHTLY
where product_pillar in ('Vodeclic','SMTPORTAL')
;
















































-- -- =================================================================================
-- drop table if exists #customer_key_sp_pcp_first_contract_month;
-- select 
--     CUSTOMER_KEY,
--     PRODUCT_PILLAR,
--     case 
--         when [PRODUCT_PILLAR] = 'Skillsoft' then 1
--         else 0
--     end as Skillport_flag,
--     case 
--         when [PRODUCT_PILLAR] = 'Percipio' then 1
--         else 0
--     end as Percipio_flag,
--     -- min(FISCAL_YEAR_MTH) as customer_product_first_fiscal_yrmth,
--     min(MTH_START_DATE) as customer_product_first_month,
--     max(MTH_START_DATE) as customer_product_last_month
-- into #customer_key_sp_pcp_first_contract_month
-- from #FTA_sp_pcp_history
-- group by CUSTOMER_KEY, PRODUCT_PILLAR
-- order BY CUSTOMER_KEY, customer_product_first_month, PRODUCT_PILLAR
-- ;


-- drop table if exists #FTA_customer_sp_pcp_info;
-- select 
--     customer_key,
--     max(
--         case 
--             when PRODUCT_PILLAR = 'Skillsoft'
--             then customer_product_first_month
--         end
--     ) as skillport_first_month, 
--     max(
--         case 
--             when PRODUCT_PILLAR = 'Percipio'
--             then customer_product_first_month
--         end
--     ) as Percipio_first_month, 

--     max(
--         case 
--             when PRODUCT_PILLAR = 'Skillsoft'
--             then customer_product_last_month
--         end
--     ) as skillport_last_month, 
--     max(
--         case 
--             when PRODUCT_PILLAR = 'Percipio'
--             then customer_product_last_month
--         end
--     ) as Percipio_last_month, 

--     max(skillport_flag) as skillport_ever_flag,
--     max(percipio_flag) as percipio_ever_flag
-- into #FTA_customer_sp_pcp_info
-- from #customer_key_sp_pcp_first_contract_month
-- group by customer_key
-- order by CUSTOMER_KEY
-- ;
-- -- =================================================================================

-- drop table if exists sandbox.dbo.rg_FTA_customer_platform_history_info;
-- select 
--     FTA_platform_history_table.*,
--     -- FTA_platform_info_table.skillport_first_fiscal_yrmth,
--     FTA_platform_info_table.skillport_first_month, 
--     FTA_platform_info_table.skillport_last_month, 
--     -- FTA_platform_info_table.percipio_first_fiscal_yrmth,
--     FTA_platform_info_table.percipio_first_month,
--     FTA_platform_info_table.percipio_last_month, 
--     FTA_platform_info_table.skillport_ever_flag,
--     FTA_platform_info_table.percipio_ever_flag
-- into sandbox.dbo.rg_FTA_customer_platform_history_info
-- from #FTA_sp_pcp_history as FTA_platform_history_table
-- LEFT JOIN #FTA_customer_sp_pcp_info as FTA_platform_info_table
-- ON FTA_platform_history_table.CUSTOMER_KEY = FTA_platform_info_table.CUSTOMER_KEY
-- ;
























-- =====================================

select
    first_product_platform,
    second_product_platform,
    third_product_platform,
    count(distinct CUSTOMER_KEY) as cnt_d_customer
from #FTA_customer_platform_summary
where first_product_platform = 'SP'
group by 
    first_product_platform,
    second_product_platform,
    third_product_platform
order by 
    first_product_platform,
    second_product_platform,
    third_product_platform
;

-- drop table if exists sandbox.dbo.rg_FTA_customer_platform_summary;
-- select *
-- into sandbox.dbo.rg_FTA_customer_platform_summary
-- from #FTA_customer_sp_pcp_info
-- ;




-- select 
--     skillport_flag,
--     percipio_flag,
--     count(distinct customer_key) as cnt_d_customer
-- from #FTA_customer_sp_pcp_summary
-- group by skillport_flag, percipio_flag
-- -- order by CUSTOMER_KEY
-- ;


-- are there opps that start with skillport after percipio?
-- there are


-- select 
--     skillport_first_fiscal_yrmth,
--     percipio_first_fiscal_yrmth,
--     skillport_ever_flag, 
--     percipio_ever_flag,
--     count(distinct CUSTOMER_KEY) as cnt_d_customer
-- from #FTA_customer_sp_pcp_info
-- where skillport_first_fiscal_yrmth is not NULL
-- group by 
--     skillport_ever_flag, 
--     percipio_ever_flag,
--     skillport_first_fiscal_yrmth,
--     percipio_first_fiscal_yrmth
-- order by skillport_first_fiscal_yrmth
-- ;


-- select *
-- from #FTA_customer_sp_pcp_info
-- where skillport_ever_flag = 1 
-- and skillport_first_fiscal_yrmth < Percipio_first_fiscal_yrmth
-- order by CUSTOMER_KEY
-- ;